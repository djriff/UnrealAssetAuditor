﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TestReferenceCommandlet.h"

// Engine
#include "AssetRegistryModule.h"
#include "Developer/AssetTools/Public/AssetToolsModule.h"

DEFINE_LOG_CATEGORY_STATIC(LogCommandlet, Log, All);

int32 UTestReferenceCommandlet::Main(const FString& Params)
{

	FindAssets();

	return Super::Main(Params);
}

void UTestReferenceCommandlet::FindAssets()
{
	auto& AssetRegistryModule = FModuleManager::Get().LoadModuleChecked<FAssetRegistryModule>(TEXT("AssetRegistry"));
	auto& AssetRegistry = AssetRegistryModule.Get();

	// Get all the asset in our content folders
	AssetRegistry.SearchAllAssets(true);
	while (AssetRegistry.IsLoadingAssets())
	{
		AssetRegistry.Tick(1.0f);
	}

	TArray<FAssetData> Assets;
	UClass* TargetClass = AActor::StaticClass();
	AssetRegistry.GetAssetsByClass(TargetClass->GetFName(), Assets, true);

	for(const FAssetData& Asset : Assets)
	{
		AActor* Instance  = Cast<AActor>(Asset.GetAsset());
		if(!Instance)
		{
			// Log this
			continue;
		}

		TArray<UObject*> ReferencedObjects;
		Instance->GetReferencedContentObjects(ReferencedObjects);

		UE_LOG(LogCommandlet, Log, TEXT("Asset %s has %i Referenced Objects"),
			*GetNameSafe(Instance),
			ReferencedObjects.Num()
		);
		
		for(UObject* ReferencedObject : ReferencedObjects)
		{
			if(!ReferencedObject)
			{
				// Log this
				continue;
			}
			
			FResourceSizeEx ResourceSizeEx;
			ReferencedObject->GetResourceSizeEx(ResourceSizeEx);

			FString ResourceSizeMode;
			switch(ResourceSizeEx.GetResourceSizeMode())
			{
				case EResourceSizeMode::Exclusive:
					ResourceSizeMode = TEXT("Exlusive");
					break;
				case EResourceSizeMode::EstimatedTotal:
				default:
					ResourceSizeMode = TEXT("EstimatedTotal");
					break;

			}

			UE_LOG(LogCommandlet, Log, TEXT("[%s]: Uses %s Resource Size Mode. It has:\n%zu Dedicated System Memory Bytes\n%zu SharedSystemMemoryBytes\n%zu Dedicated Video Memory Bytes\n%zu Shared Video Memory Bytes\n%zu Unknown Memory Bytes\n%zu Total Memory Bytes "),
				*GetNameSafe(ReferencedObject),
				*ResourceSizeMode,
				ResourceSizeEx.GetDedicatedSystemMemoryBytes(),
				ResourceSizeEx.GetSharedSystemMemoryBytes(),
				ResourceSizeEx.GetDedicatedVideoMemoryBytes(),
				ResourceSizeEx.GetSharedVideoMemoryBytes(),
				ResourceSizeEx.GetUnknownMemoryBytes(),
				ResourceSizeEx.GetTotalMemoryBytes()
			);
		}
	}

}

void UTestReferenceCommandlet::FindAssetReferences()
{
	FindAssets();
}
