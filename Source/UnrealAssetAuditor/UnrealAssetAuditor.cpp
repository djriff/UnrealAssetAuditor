// Copyright Epic Games, Inc. All Rights Reserved.

#include "UnrealAssetAuditor.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UnrealAssetAuditor, "UnrealAssetAuditor" );
 