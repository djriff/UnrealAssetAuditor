﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Commandlets/Commandlet.h"
#include "TestReferenceCommandlet.generated.h"

/**
 * 
 */
UCLASS()
class UNREALASSETAUDITOR_API UTestReferenceCommandlet : public UCommandlet
{
	GENERATED_BODY()

	// ~ begin Commmandlet overrides
	virtual int32 Main(const FString& Params) override;
	// ~ end Commmandlet overrides

	void FindAssets();

	UFUNCTION(Exec)
	void FindAssetReferences();
};
