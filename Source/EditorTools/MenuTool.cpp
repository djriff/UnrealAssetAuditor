﻿#include "MenuTool.h"
#include "AssetAuditor.h"


#define LOCTEXT_NAMESPACE "MenuTool"

class MenuToolCommands : public TCommands<MenuToolCommands>
{

public:
	
	MenuToolCommands()
	   : TCommands<MenuToolCommands>
		(
		   TEXT("MenuTool"), // Context name for fast lookup
		   FText::FromString("Asset Audit tool"), // Context name for displaying
		   NAME_None,   // No parent context
		   FEditorStyle::GetStyleSetName() // Icon Style Set
	   )
	{
	}

	virtual void RegisterCommands() override
	{
		UI_COMMAND(MenuCommand1, "Asset Audit", "Audits Assets in the Project for References.", EUserInterfaceActionType::Button, FInputGesture());

	}

public:
	TSharedPtr<FUICommandInfo> MenuCommand1;
};

MenuTool::MenuTool()
{
	AssetAuditor = nullptr;
}

void MenuTool::OnStartupModule()
{
	CommandList = MakeShareable(new FUICommandList);
	MenuToolCommands::Register();
	MapCommands();
	FEditorTools::Get().AddMenuExtension(
		FMenuExtensionDelegate::CreateRaw(this, &MenuTool::MakeMenuEntry),
		FName("Section_1"),
		CommandList);
}

void MenuTool::OnShutdownModule()
{
	MenuToolCommands::Unregister();
}

void MenuTool::MakeMenuEntry(FMenuBuilder& menuBuilder)
{
	menuBuilder.AddMenuEntry(MenuToolCommands::Get().MenuCommand1);
}

void MenuTool::MapCommands()
{
	const auto& Commands = MenuToolCommands::Get();

	CommandList->MapAction(
		Commands.MenuCommand1,
		FExecuteAction::CreateSP(this, &MenuTool::MenuCommand1),
		FCanExecuteAction());
}

void MenuTool::MenuCommand1()
{
	FGCScopeGuard ScopeGuard;
	
	AssetAuditor = NewObject<UAssetAuditor>();
	AssetAuditor->FindAssets(true, true);
}

#undef LOCTEXT_NAMESPACE
