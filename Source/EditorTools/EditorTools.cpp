﻿#include "EditorTools.h"
#include "MenuTool.h"

// Engine
#include "LevelEditor.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_GAME_MODULE(FEditorTools, EditorTools);

void FEditorTools::StartupModule()
{
	if (!IsRunningCommandlet())
	{
		FLevelEditorModule& LevelEditorModule = FModuleManager::LoadModuleChecked<FLevelEditorModule>("LevelEditor");
		LevelEditorMenuExtensibilityManager = LevelEditorModule.GetMenuExtensibilityManager();
		MenuExtender = MakeShareable(new FExtender);
		MenuExtender->AddMenuBarExtension("Window", EExtensionHook::After, NULL, FMenuBarExtensionDelegate::CreateRaw(this, &FEditorTools::MakePulldownMenu));
		LevelEditorMenuExtensibilityManager->AddExtender(MenuExtender);
	}
	
	IEditorModuleInterface::StartupModule();
}

void FEditorTools::ShutdownModule()
{
	IEditorModuleInterface::ShutdownModule();
}

void FEditorTools::AddModuleListeners()
{
	ModuleListeners.Add(MakeShareable(new MenuTool));
}

void FEditorTools::AddMenuExtension(const FMenuExtensionDelegate& extensionDelegate, FName extensionHook,
	const TSharedPtr<FUICommandList>& CommandList, EExtensionHook::Position position)
{
	MenuExtender->AddMenuExtension(extensionHook, position, CommandList, extensionDelegate);
}

void FEditorTools::MakePulldownMenu(FMenuBarBuilder& menuBuilder)
{
	menuBuilder.AddPullDownMenu(
	FText::FromString("Asset Audit Menu"),
	FText::FromString("Open the Asset Audit menu"),
	FNewMenuDelegate::CreateRaw(this, &FEditorTools::FillPulldownMenu),
	"AssetAudit",
	FName(TEXT("Asset Audit Menu"))
);
}

void FEditorTools::FillPulldownMenu(FMenuBuilder& menuBuilder)
{
	// just a frame for tools to fill in
	menuBuilder.BeginSection("AssetSection", FText::FromString("Section 1"));
	menuBuilder.AddMenuSeparator(FName("Section_1"));
	menuBuilder.EndSection();

	menuBuilder.BeginSection("AssetSection", FText::FromString("Section 2"));
	menuBuilder.AddMenuSeparator(FName("Section_2"));
	menuBuilder.EndSection();
}
