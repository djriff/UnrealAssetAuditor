﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "AssetAuditor.generated.h"

/**
 * 
 */
UCLASS()
class EDITORTOOLS_API UAssetAuditor : public UObject
{
	GENERATED_BODY()

public:

	UAssetAuditor();

	/**
	 *	Finds referenced assets for all assets under the content folder.
	 *	@warning This will block the thread it's called on for a very very long time.
	 *
	 *	@param bWriteToJSON					If the results should be written to a JSON file. Stored in the Saved folder
	 *	@param bLogDetailedAuditResults		If the logging should be detailed. This will log every reference separated by
	 *										internal and external references.
	 */
	void FindAssets(bool bWriteToJSON = true, bool bLogDetailedAuditResults = false);

	void OutputToJSONFile();

	void LogReferencedObject();

	void AddMemoryBytesToObject(TSharedPtr<class FJsonObject>& ObjectToAdd, UObject* ObjectToCheck);

private:

	UPROPERTY(Transient)
	TArray<struct FAssetData> Assets;
};
