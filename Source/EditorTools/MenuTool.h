﻿#pragma once
#include "EditorTools.h"

class MenuTool : public IEditorModuleListenerInterface, public TSharedFromThis<MenuTool>
{
	
public:
	MenuTool();
	virtual ~MenuTool() {}

	virtual void OnStartupModule() override;
	virtual void OnShutdownModule() override;

	void MakeMenuEntry(FMenuBuilder &menuBuilder);

protected:
	TSharedPtr<FUICommandList> CommandList;

	void MapCommands();

	// UI Command functions
	void MenuCommand1();

private:
	
	class UAssetAuditor* AssetAuditor;
	
};
