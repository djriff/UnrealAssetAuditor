﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "AssetAuditor.h"

// Test Project
#include "UnrealAssetAuditor/TestProjectCharacter.h"

// Engine
#include "AssetRegistryModule.h"
#include "Developer/AssetTools/Public/AssetToolsModule.h"
#include "Dom/JsonObject.h"
#include "Misc/FileHelper.h"
#include "Serialization/JsonSerializer.h"
#include "Serialization/JsonWriter.h"

DEFINE_LOG_CATEGORY_STATIC(LogAssetAuditor, Log, All);

UAssetAuditor::UAssetAuditor()
{
}

void UAssetAuditor::FindAssets(bool bWriteToJSON, bool bLogAuditResults)
{
	UE_LOG(LogAssetAuditor, Log, TEXT("Beginning Asset Audit"));
	
	const FAssetRegistryModule& AssetRegistryModule = FModuleManager::Get().LoadModuleChecked<FAssetRegistryModule>(TEXT("AssetRegistry"));
	IAssetRegistry& AssetRegistry = AssetRegistryModule.Get();

	// Get all the asset in our content folders
	AssetRegistry.SearchAllAssets(true);
	while (AssetRegistry.IsLoadingAssets())
	{
		AssetRegistry.Tick(1.0f);
	}
	
	AssetRegistry.GetAssetsByPath(FName("/Game"), Assets, true);
	//AssetRegistry.GetAssetsByClass(TargetClass->GetFName(), Assets, true);

	UE_LOG(LogAssetAuditor, Log, TEXT("Found %i Assets to Audit"), Assets.Num());

	if(bWriteToJSON)
	{
		OutputToJSONFile();
	}

	if(bLogAuditResults)
	{
		LogReferencedObject();
	}

}

void UAssetAuditor::OutputToJSONFile()
{
	FGCScopeGuard ScopeGuard;
	
	TSharedPtr<FJsonObject> JsonObject = MakeShared<FJsonObject>();
	TArray<TSharedPtr<FJsonValue>> AssetsList;
	AssetsList.Reserve(Assets.Num());
	TSharedPtr<FJsonValueNumber> AssetCount = MakeShared<FJsonValueNumber>(Assets.Num());
	
	JsonObject->SetField(TEXT("Asset Count"), AssetCount);

	for(const FAssetData& Asset: Assets)
	{
		TSharedPtr<FJsonObject> AssetObject = MakeShared<FJsonObject>();

		UObject* Instance  = Cast<UObject>(Asset.GetAsset());
		if(!Instance)
		{
			UE_LOG(LogAssetAuditor, Warning, TEXT("Asset %s will not be stored in JSON data as it is not UObject based."), *Asset.AssetName.ToString());
			continue;
		}

		FString PrimaryAssetString;
		
		FPrimaryAssetId PrimaryAssetId = Asset.GetPrimaryAssetId();
		if(PrimaryAssetId.IsValid())
		{
			PrimaryAssetString = PrimaryAssetId.ToString();
		}

		TSharedPtr<FJsonValueString> PrimaryAssetJsonString = MakeShared<FJsonValueString>(PrimaryAssetString);
		
		// Add to Json Object Array
		FString AssetName = Asset.AssetName.ToString();
		
		TSharedPtr<FJsonValueString> JsonAssetName = MakeShared<FJsonValueString>(AssetName);
		
		TArray<FReferencerInformation> InternalReferencers;
		TArray<FReferencerInformation> ExternalReferencers;		
		Instance->RetrieveReferencers(&InternalReferencers, &ExternalReferencers);

		TSharedPtr<FJsonValueNumber> InternalReferenceCount = MakeShared<FJsonValueNumber>(InternalReferencers.Num());
		TSharedPtr<FJsonValueNumber> ExternalReferenceCount = MakeShared<FJsonValueNumber>(ExternalReferencers.Num());

		TSharedPtr<FJsonObject> InternalReferencesObject = MakeShared<FJsonObject>();
		TSharedPtr<FJsonObject> ExternalReferencesObject = MakeShared<FJsonObject>();

		TArray<TSharedPtr<FJsonValue>> InternalReferenceList;
		InternalReferenceList.Reserve(InternalReferencers.Num());

		TArray<TSharedPtr<FJsonValue>> ExternalReferenceList;
		ExternalReferenceList.Reserve(ExternalReferencers.Num());		

		for(const FReferencerInformation& ReferenceInformation : InternalReferencers)
		{
			TSharedPtr<FJsonObject> InternalReferenceObject = MakeShared<FJsonObject>();
			TSharedPtr<FJsonValueString> ReferenceObjectName = MakeShared<FJsonValueString>(GetNameSafe(ReferenceInformation.Referencer));
			TSharedPtr<FJsonValueString> FullName = MakeShared<FJsonValueString>(ReferenceInformation.Referencer->GetFullName());
			TSharedPtr<FJsonValueNumber> ReferenceCount = MakeShared<FJsonValueNumber>(ReferenceInformation.TotalReferences);

			UObject* Referencer = ReferenceInformation.Referencer;
			AddMemoryBytesToObject(InternalReferenceObject, Referencer);

			TArray<TSharedPtr<FJsonValue>> ReferencedPropertyNames;
			ReferencedPropertyNames.Reserve(ReferenceInformation.ReferencingProperties.Num());

			for(int32 i = 0; i < ReferenceInformation.TotalReferences; i++)
			{
				if(i < ReferenceInformation.ReferencingProperties.Num())
				{
					const FProperty* PropertyReferencer = ReferenceInformation.ReferencingProperties[i];
					TSharedPtr<FJsonValueString> ReferenceName = MakeShared<FJsonValueString>(GetFullNameSafe(PropertyReferencer));
					ReferencedPropertyNames.Add(ReferenceName);
				}
				else
				{
					TSharedPtr<FJsonValueString> ReferenceName = MakeShared<FJsonValueString>(TEXT("Native refernce"));
					ReferencedPropertyNames.Add(ReferenceName);
				}
			}

			TSharedPtr<FJsonValueArray> ReferencedProperties = MakeShared<FJsonValueArray>(ReferencedPropertyNames);
			
			InternalReferenceObject->SetField(TEXT("Reference Obejct Name"), ReferenceObjectName);
			InternalReferenceObject->SetField(TEXT("Full Name"), FullName);
			InternalReferenceObject->SetField(TEXT("Property Reference Count"), ReferenceCount);
			InternalReferenceObject->SetArrayField(TEXT("Referenced Properties"), ReferencedPropertyNames);

			TSharedPtr<FJsonValueObject> InternalReferenceValue = MakeShared<FJsonValueObject>(InternalReferenceObject);
			InternalReferenceList.Add(InternalReferenceValue);
		}
		
		for(const FReferencerInformation& ReferenceInformation : ExternalReferencers)
		{
			TSharedPtr<FJsonObject> ExternalReferenceObject = MakeShared<FJsonObject>();
			bool bIsRootReference = false;
			bool bIsNativeReference = false;
			bool bIsStandaloneReference = false;

			if(ReferenceInformation.Referencer->IsRooted())
			{
				bIsRootReference = true;
			}

			if(ReferenceInformation.Referencer->IsNative())
			{
				bIsNativeReference = true;
			}

			if(ReferenceInformation.Referencer->HasAnyFlags(RF_Standalone))
			{
				bIsStandaloneReference = true;
			}

			TSharedPtr<FJsonValueString> ReferenceObjectName = MakeShared<FJsonValueString>(GetNameSafe(ReferenceInformation.Referencer));
			TSharedPtr<FJsonValueString> FullName = MakeShared<FJsonValueString>(ReferenceInformation.Referencer->GetFullName());
			TSharedPtr<FJsonValueNumber> ReferenceCount = MakeShared<FJsonValueNumber>(ReferenceInformation.TotalReferences);
			TSharedPtr<FJsonValueBoolean> IsRootReference = MakeShared<FJsonValueBoolean>(bIsRootReference);
			TSharedPtr<FJsonValueBoolean> IsNativeReference = MakeShared<FJsonValueBoolean>(bIsNativeReference);
			TSharedPtr<FJsonValueBoolean> IsStandaloneReference = MakeShared<FJsonValueBoolean>(bIsStandaloneReference);

			UObject* Referencer = ReferenceInformation.Referencer;
			AddMemoryBytesToObject(ExternalReferenceObject, Referencer);
			
			TArray<TSharedPtr<FJsonValue>> ReferencedPropertyNames;
			ReferencedPropertyNames.Reserve(ReferenceInformation.ReferencingProperties.Num());

			for(int32 i = 0; i < ReferenceInformation.TotalReferences; i++)
			{
				if(i < ReferenceInformation.ReferencingProperties.Num())
				{
					const FProperty* PropertyReferencer = ReferenceInformation.ReferencingProperties[i];
					TSharedPtr<FJsonValueString> ReferenceName = MakeShared<FJsonValueString>(GetFullNameSafe(PropertyReferencer));
					ReferencedPropertyNames.Add(ReferenceName);
				}
				else
				{
					TSharedPtr<FJsonValueString> ReferenceName = MakeShared<FJsonValueString>(TEXT("Native refernce"));
					ReferencedPropertyNames.Add(ReferenceName);
				}				
			}

			TSharedPtr<FJsonValueArray> ReferencedProperties = MakeShared<FJsonValueArray>(ReferencedPropertyNames);
			
			ExternalReferenceObject->SetField(TEXT("Reference Obejct Name"), ReferenceObjectName);
			ExternalReferenceObject->SetField(TEXT("Full Name"), FullName);
			ExternalReferenceObject->SetField(TEXT("Property Reference Count"), ReferenceCount);
			ExternalReferenceObject->SetField(TEXT("Is Root Reference"), IsRootReference);
			ExternalReferenceObject->SetField(TEXT("Is Native Reference"), IsNativeReference);
			ExternalReferenceObject->SetField(TEXT("Is Standalone Reference"), IsStandaloneReference);
			ExternalReferenceObject->SetArrayField(TEXT("Referenced Properties"), ReferencedPropertyNames);
			

			TSharedPtr<FJsonValueObject> ExternalReferenceValue = MakeShared<FJsonValueObject>(ExternalReferenceObject);
			ExternalReferenceList.Add(ExternalReferenceValue);
		}

		TSharedPtr<FJsonObject> AssetSubObject = MakeShared<FJsonObject>();

		TSharedPtr<FJsonValueString> AssetFullName = MakeShared<FJsonValueString>(Asset.GetFullName());

		AssetSubObject->SetField("Asset Path", AssetFullName);
		AssetSubObject->SetField(TEXT("Primary Asset Id"), PrimaryAssetJsonString);

		AddMemoryBytesToObject(AssetSubObject, Instance);

		AssetSubObject->SetField(TEXT("Internal Reference Count"), InternalReferenceCount);
		AssetSubObject->SetField(TEXT("External Reference Count"), ExternalReferenceCount);
		
		AssetSubObject->SetArrayField(TEXT("Internal References"), InternalReferenceList);
		AssetSubObject->SetArrayField(TEXT("External References"), ExternalReferenceList);
		
		TSharedPtr<FJsonValueObject> OutAssetObject = MakeShared<FJsonValueObject>(AssetSubObject);
		
		AssetObject->SetField(AssetName, OutAssetObject);
		
		TSharedPtr<FJsonValueObject> FormattedAssetObject = MakeShared<FJsonValueObject>(AssetObject);
		AssetsList.Add(FormattedAssetObject);		
	}

	JsonObject->SetArrayField(TEXT("Assets"), AssetsList);

	FString AssetAuditString;
	TSharedRef<TJsonWriter<>> Writer = TJsonWriterFactory<>::Create(&AssetAuditString);
	FJsonSerializer::Serialize(JsonObject.ToSharedRef(), Writer);
	
	FDateTime CurrentDateTime = FDateTime::Now();	
	
	const FString FilePath = FPaths::ProjectSavedDir() + FString::Printf(TEXT("AssetAudit_%s.json"), *CurrentDateTime.ToString());
	FFileHelper::SaveStringToFile(AssetAuditString, *FilePath);
	
}

void UAssetAuditor::LogReferencedObject()
{
	if(Assets.Num() <= 0)
	{
		UE_LOG(LogAssetAuditor, Error, TEXT("[%s]: Was called without any assets."), *FString(__FUNCTION__));
		return;
	}
	
	for(const FAssetData& Asset : Assets)
	{
		UObject* Instance  = Cast<UObject>(Asset.GetAsset());
		if(!Instance)
		{
			// Log this
			continue;
		}

		TArray<FReferencerInformation> InternalReferencers;
		TArray<FReferencerInformation> ExternalReferencers;		
		Instance->RetrieveReferencers(&InternalReferencers, &ExternalReferencers);
		
		FReferencerInformationList* ReferencerInformationList = new FReferencerInformationList(InternalReferencers, ExternalReferencers);
		
		FStringOutputDevice Ar;
		Instance->OutputReferencers(Ar, ReferencerInformationList);

		UE_LOG(LogAssetAuditor, Log, TEXT("Asset %s has %i Intenally Referenced Objects aand %i Externally Referenced Objects"),
			*GetNameSafe(Instance),
			InternalReferencers.Num(),
			ExternalReferencers.Num()
		);

		UE_LOG(LogAssetAuditor, Log, TEXT("%s"), *Ar);

		delete ReferencerInformationList;
	}
}

void UAssetAuditor::AddMemoryBytesToObject(TSharedPtr<FJsonObject>& ObjectToAdd, UObject* ObjectToCheck)
{
	TSharedPtr<FResourceSizeEx> ObjectResourceSizeEx = MakeShared<FResourceSizeEx>();
	ObjectToCheck->GetResourceSizeEx(*ObjectResourceSizeEx);
	
	uint64 ObjectTotalMemorySize = ObjectResourceSizeEx->GetTotalMemoryBytes();
	uint64 ObjectUnknownMemorySize = ObjectResourceSizeEx->GetUnknownMemoryBytes();
	uint64 ObjectDedicatedSystemMemorySize = ObjectResourceSizeEx->GetDedicatedSystemMemoryBytes();
	uint64 ObjectDedicatedVideoMemorySize = ObjectResourceSizeEx->GetDedicatedVideoMemoryBytes();
	uint64 ObjectSharedSystemMemorySize = ObjectResourceSizeEx->GetSharedSystemMemoryBytes();
	uint64 ObjectSharedVideoMemorySize = ObjectResourceSizeEx->GetSharedVideoMemoryBytes();
	const EResourceSizeMode::Type ObjectResourceSizeMode = ObjectResourceSizeEx->GetResourceSizeMode();
	
	FString ObjectResourceSizeModeString;
	switch(ObjectResourceSizeMode)
	{
	case EResourceSizeMode::Exclusive:
		ObjectResourceSizeModeString = TEXT("Exclusive");
		break;
	case EResourceSizeMode::EstimatedTotal: 
	default:
		ObjectResourceSizeModeString = TEXT("Estimated Total");
	}

	const TSharedPtr<FJsonValueNumber> ObjectTotalMemory = MakeShared<FJsonValueNumber>(ObjectTotalMemorySize);
	const TSharedPtr<FJsonValueNumber> ObjectUnknownMemory = MakeShared<FJsonValueNumber>(ObjectUnknownMemorySize);
	const TSharedPtr<FJsonValueNumber> ObjectDedicatedSystemMemory = MakeShared<FJsonValueNumber>(ObjectDedicatedSystemMemorySize);
	const TSharedPtr<FJsonValueNumber> ObjectDedicatedVideoMemory = MakeShared<FJsonValueNumber>(ObjectDedicatedVideoMemorySize);
	const TSharedPtr<FJsonValueNumber> ObjectSharedSystemMemory = MakeShared<FJsonValueNumber>(ObjectSharedSystemMemorySize);
	const TSharedPtr<FJsonValueNumber> ObjectSharedVideoMemory = MakeShared<FJsonValueNumber>(ObjectSharedVideoMemorySize);
	const TSharedPtr<FJsonValueString> ObjectResourceSizeModeType = MakeShared<FJsonValueString>(ObjectResourceSizeModeString);

	ObjectToAdd->SetField(TEXT("Resources Size Mode Type"), ObjectResourceSizeModeType);
	ObjectToAdd->SetField(TEXT("Dedicated System Memory Bytes"), ObjectDedicatedSystemMemory);
	ObjectToAdd->SetField(TEXT("Dedicated Video Memory Bytes"), ObjectDedicatedVideoMemory);
	ObjectToAdd->SetField(TEXT("Shared System Memory Bytes"), ObjectSharedSystemMemory);
	ObjectToAdd->SetField(TEXT("Shared Video Memory Bytes"), ObjectSharedVideoMemory);
	ObjectToAdd->SetField(TEXT("Unknown Memory Bytes"), ObjectUnknownMemory);
	ObjectToAdd->SetField(TEXT("Total Memory Bytes"), ObjectTotalMemory);
}
